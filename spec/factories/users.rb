FactoryBot.define do
  factory :user do
    first_name { "MyString" }
    last_name { "MyString" }
    age { 1 }
    phone { "MyString" }
  end
end
