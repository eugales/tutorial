source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.4.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.2' #, github: 'rails/rails', branch: 'master'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.4.4', '< 0.6.0'
gem 'native_enum', github: 'fruwe/native_enum', branch: 'mcls/rails-5.1'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'hiredis'
gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'versionist'

gem 'activestorage-validator'

gem 'cancancan', '~> 3.0'
gem 'devise'
gem 'devise-jwt'
gem 'devise-two-factor'
gem 'recaptcha', '~> 4.0'
gem 'rest-client', '~> 2.0', '>= 2.0.2'
gem 'rolify'
# Auto find record by id in controllers
gem 'decent_exposure'
# Validation for files
gem 'file_validators'
# API documentation generation
gem 'apitome'
gem 'rspec_api_documentation'

gem 'geokit'
gem 'geokit-rails'

gem 'bcrypt', '~> 3.1.7'

gem 'daemons'
gem 'prawn'

# Statuses
gem 'aasm'

gem 'bunny', '>= 2.6.3'
gem 'hashie'

# Validation utils
gem 'validates_iban'
gem 'iso-swift', github: 'rwehresmann/iso-swift', branch: 'master'
gem 'dry-validation', '1.2.1'
gem 'sanitize', '~> 5.0.0'

# Database locking
gem 'with_advisory_lock'
# Retry blocks
gem 'retriable', '~> 3.1'

# Money related gems
gem 'money-open-exchange-rates'
gem 'money-rails', '~>1.12'
# Schedule tasks
gem 'whenever', require: false

# Pagination
gem 'will_paginate', '~> 3.1.0'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

gem "comfortable_mexican_sofa", "~> 2.0.0"

# Do not upgrade this gem(specs/acceptance/registrations_spec.rb falls)
gem 'active_model_serializers', '~> 0.9.0'

gem "haml-rails", "~> 2.0"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'guard-rspec', require: false
  gem 'guard-livereload'
end

group :development do
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails-db'
  gem 'capistrano-rake', require: false
  gem 'capistrano-rvm'
  gem 'capistrano-upload-config'
  gem 'capistrano3-nginx'
  gem 'capistrano3-puma'
  gem 'rspec-rails'
end

group :test do
  gem 'shoulda-matchers'
  gem 'rails-controller-testing'
  gem 'database_cleaner'
  gem 'rubocop', '~> 0.7'
  gem 'json_spec'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
